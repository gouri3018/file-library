#include "file.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#define MAX 20
#define READ 100
#define WRITE 200
#define RDWR 300
#define ENDOFFILE 500

MYFILE _stdin =  { 0 , 1};
MYFILE _stdout =  { 1 , 1};
MYFILE _stderr =  { 2 , 1};

MYFILE _fileinfo[MAX];
static int _index = -1;

//fopen
MYFILE *myfopen(char *filename, char *mode) {
	MYFILE *_fp;
	int _check, _counter = 0;
	_size _tmp;
	_index++;
	if(_index >= MAX || _fileinfo[_index]._usage == _INUSE) {
		for(_check = 0; _check < MAX; _check++) {
			if(_fileinfo[_check]._usage == _NOTINUSE) {
				_index = _check;
				_counter++;
				break;
			}
		}
		if(_counter == 0) {
			errno = ENOMEM;
			return NULL;
		}
	}
	_fp = &_fileinfo[_index];
	_fp->_usage = _INUSE;
	_fp->_arg = mode;
	_fp->_buffindex = 0;
	_fp->_indicator = 0;
	_fp->_readcount = 0;
	_fp->_flagsize = 1;
	for(_tmp = 0; _tmp < BUFMAX; _tmp++) {
		_fp->_buffer[_tmp] = '\0';
	}
	_fp->_currchar = 0;
	if(!strcmp(mode, "r") || !strcmp(mode, "rb"))  {
		_fp->_fileno = open(filename, O_RDONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
		if(_fp->_fileno == -1) {
			return NULL;
		}
	}
	else if(!strcmp(mode, "w") || !strcmp(mode, "wb")) {
		_fp->_fileno = open(filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
		if(_fp->_fileno == -1) {
			return NULL;
		}
	}
	else if(!strcmp(mode, "a") || !strcmp(mode, "ab")) {
		_fp->_fileno = open(filename, O_APPEND | O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
		if(_fp->_fileno == -1) {
			return NULL;
		}
	}
	else if(!strcmp(mode, "a+") || !strcmp(mode, "a+b") || !strcmp(mode, "ab+")) {
		_fp->_fileno = open(filename, O_APPEND | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
		if(_fp->_fileno == -1) {
			return NULL;
		}
	}
	else if(!strcmp(mode, "w+") || !strcmp(mode, "w+b") || !strcmp(mode, "wb+")) {
		_fp->_fileno = open(filename, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
		if(_fp->_fileno == -1) {
			return NULL;
		}
	}
	else if(!strcmp(mode, "r+") || !strcmp(mode, "r+b") || !strcmp(mode, "rb+")) { 
		_fp->_fileno = open(filename, O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
		if(_fp->_fileno == -1) {
			return NULL;
		}
	}
	else {
		return NULL;
	}	
	return _fp;
}

void _bufftofile(MYFILE *stream) {
	_size _tmp;
	if(stream->_buffindex != 0 && stream->_indicator != READ) {
		if(stream->_indicator == RDWR || stream->_indicator == ENDOFFILE || stream->_readcount == BUFMAX) {
			lseek(stream->_fileno, -stream->_readcount, SEEK_CUR);
		}
		for(_tmp = 0; _tmp < BUFMAX; _tmp++) {
			if(stream->_buffer[_tmp] == '\0' && stream->_flagsize == 1) {
				break;
			}
			write(stream->_fileno, &stream->_buffer[_tmp], 1);
		}
	}
	for(_tmp = 0; _tmp < BUFMAX; _tmp++) {
		stream->_buffer[_tmp] = '\0';
	}
	stream->_currchar = stream->_buffindex = stream->_readcount =  0;
}

//fclose
int myfclose(MYFILE *stream) {
	int _flag;
	if(stream == NULL)
		return -1;
	stream->_currchar = stream->_buffindex;
	_bufftofile(stream);	//write remaining data from buffer into file
	_flag = close(stream->_fileno);
	stream->_usage = _NOTINUSE;
	stream->_arg = NULL;
	return _flag;
}

//fread
_size myfread(void *ptr, _size size, _size n, MYFILE *stream) {
	_size _count = 0, _tmp = 0, _cal;
	int _flag = 0, _flagread;
	char _ch;
	char _str[size * n + 1];
	stream->_flagsize = size;
	if(stream->_indicator == WRITE)
		stream->_indicator = RDWR;
	else if(stream->_indicator != ENDOFFILE)
		stream->_indicator = READ;
	if(!stream->_buffindex) {
		if(stream->_fileno != 0 && stream->_indicator != READ)
			_bufftofile(stream);
			do {
				_flagread = read(stream->_fileno, &(stream->_buffer[stream->_buffindex++]), sizeof(char));
				if(!_flagread) {
					stream->_indicator = ENDOFFILE;
					stream->_buffer[stream->_buffindex - 1] = '\0';
					break;
				}
				else {
					stream->_readcount++;
				}
			}while(stream->_buffindex != BUFMAX && (stream->_fileno != 0 || 
				stream->_buffer[stream->_buffindex - 1] != '\n' ));
		}	
		if((size * n) > (_size)(stream->_buffindex - stream->_currchar)) {
			for(_tmp = 0; _tmp < (stream->_buffindex - stream->_currchar) ; _tmp++) {
				_ch = stream->_buffer[stream->_currchar + _tmp];
				_str[_tmp] = _ch;
				if(_ch == '\0' && size == 1) {
					_flag = 1;
					break;
				}
				_count++;
			}
			if(!_flag) {
				stream->_currchar += _count;
				_cal = ((size * n) - _count);
				if(_cal % BUFMAX == 0)
					_cal /= BUFMAX;
				else
					_cal = _cal / BUFMAX + 1;
				while(_cal--) {
						_bufftofile(stream);
					if(stream->_fileno == 0) {
						while(stream->_buffindex != stream->_currchar) {
							_str[_tmp++] = stream->_buffer[stream->_currchar++];
							_count++;
						}
						stream->_buffindex = stream->_currchar = 0;
					}
					stream->_buffindex = stream->_currchar = 0;						 
						
					do {
						_flagread = read(stream->_fileno, &(stream->_buffer[stream->_buffindex++]), sizeof(char));
						if(!_flagread) {
							stream->_indicator = ENDOFFILE;
							stream->_buffer[stream->_buffindex - 1] = '\0';
							break;
						}
						else {
							stream->_readcount++;
						}
					}while(stream->_buffindex != BUFMAX && (stream->_fileno != 0 || 
						stream->_buffer[stream->_buffindex - 1] != '\n' ));
					while(stream->_buffindex != stream->_currchar) {
						if(_tmp == size * n)
							break;
						_ch = stream->_buffer[stream->_currchar++];
						_str[_tmp++] = _ch;
						if(_ch == '\0' && size == 1) {
							break;
						}
						_count++;
					}
				}
			}
		}
		else {	
			for(_tmp = 0; _tmp < (size * n); _tmp++) {
				_ch = stream->_buffer[stream->_currchar++];
				_str[_tmp] = _ch;
				if(_ch == '\0' && size == 1) {
					break;
				}
				_count++;
			}
		}		
	_str[_tmp] = '\0';
	memcpy(ptr, _str, _count);
	if(stream->_currchar == stream->_buffindex) {
		stream->_buffindex = stream->_currchar = 0;
		if(stream->_fileno == 0) {
			for(_tmp = 0; _tmp < BUFMAX; _tmp++)
				stream->_buffer[_tmp] = '\0';
		}
	}
	return _count;
}

//fwrite
_size myfwrite(void *ptr, _size size, _size n, MYFILE *stream) {
	_size _count = 0, _tmp = 0, _cal, _var;
	char _str[size * n + 1];
	int _i;
	if(stream->_fileno == 1 || stream->_fileno == 2) {
		memcpy(_str, ptr, size * n);
		for(_tmp = 0; _tmp < size * n; _tmp ++) {
			write(1, &_str[_tmp], 1);
			_count++;
		}

		return _count;
	}
	else {
		if(!strcmp(stream->_arg, "r")) {
			errno = EBADFD;
			return 0;
		}
		memcpy(_str, ptr, size * n);
		stream->_flagsize = size;
		if(!strcmp(stream->_arg, "a+")) {
			stream->_buffindex = stream->_currchar = 0;
			for(_i = 0; _i < BUFMAX; _i++)
				stream->_buffer[_i] = '\0';
		}
		if(stream->_indicator == READ)
			stream->_indicator = RDWR;
		else if(stream->_indicator != ENDOFFILE) 
			stream->_indicator = WRITE;
		if(stream->_currchar == BUFMAX)
			_bufftofile(stream);
		if(((size * n) > (_size)(stream->_buffindex - stream->_currchar) && stream->_buffindex != stream->_currchar))  {
			for(_tmp = 0; _tmp < (stream->_buffindex - stream->_currchar) ; _tmp++) {
				stream->_buffer[stream->_currchar + _tmp] = _str[_tmp];
				_count++;
			}
			_cal = ((size * n) - _count);
				if(_cal % BUFMAX == 0)
					_cal /= BUFMAX;
				else
					_cal = _cal / BUFMAX + 1;
				stream->_currchar += _tmp;
				while(_cal--) {
					_bufftofile(stream);
					for(_var = 0; _var < size * n - _tmp && _var < BUFMAX; _var++) {
						stream->_buffer[stream->_currchar++] = _str[_tmp + _var];
						_count++;
					}
					_tmp += _var;
					if(stream->_buffindex < stream->_currchar)
						stream->_buffindex = stream->_currchar;	
				}
		}
		else if(stream->_buffindex == stream->_currchar && (size * n) > (BUFMAX - stream->_currchar)) {
			for(_tmp = 0; _tmp < BUFMAX - stream->_currchar ; _tmp++) {
				stream->_buffer[stream->_currchar + _tmp] = _str[_tmp];
				_count++;
			}
			stream->_currchar += _tmp;
			_cal = ((size * n) - _count);
				if(_cal % BUFMAX == 0)
					_cal /= BUFMAX;
				else
					_cal = _cal / BUFMAX + 1;
				stream->_buffindex = stream->_currchar;
				while(_cal--) {
					_bufftofile(stream);
					for(_var = 0; _var < size * n - _tmp && _var < BUFMAX; _var++) {
						stream->_buffer[stream->_currchar++] = _str[_tmp + _var];
						_count++;
					}
					if(stream->_buffindex < stream->_currchar)
						stream->_buffindex = stream->_currchar;
					_tmp += _var;
				}
			stream->_buffindex = stream->_currchar;
		}
		else {
			for(_tmp = 0; _tmp < size * n; _tmp++) {
				stream->_buffer[stream->_currchar++] = _str[_tmp];
				_count++;
			}
			if(stream->_buffindex < stream->_currchar)
				stream->_buffindex = stream->_currchar;
		}
		return _count;
	}
	return 0;
}

//fseek
int myfseek(MYFILE *stream, long offset, int whence) {
	long _currcal, _buffcal;
	int _flaglseek;
	if(stream->_fileno == 0 || stream->_fileno == 1 || stream->_fileno == 2)
		return -1;
	if(whence == SEEK_CUR) {
		if(stream->_currchar + offset >= 0 && stream->_currchar + offset < BUFMAX) {
			if(stream->_buffindex == stream->_currchar) {
				stream->_currchar += offset;
				stream->_buffindex = stream->_currchar;
			}
			else
				stream->_currchar += offset;
		}
		else {
			_currcal = stream->_currchar;
			_buffcal = stream->_buffindex;
			stream->_currchar = stream->_buffindex;
			_bufftofile(stream);
			if(offset < 0)
				offset -= (_buffcal - _currcal);
			else
				offset -= (_buffcal - _currcal);
			_flaglseek = lseek(stream->_fileno, offset, whence);
			if(_flaglseek == -1) {
				return _flaglseek;
			}
		}
			
	}
	else if(whence == SEEK_SET || whence == SEEK_END) {
		if(offset < 0 && whence == SEEK_SET)
			return -1;
		if(stream->_buffindex != 0) {
			stream->_currchar = stream->_buffindex;
			_bufftofile(stream);
		}
		if(offset == 0 && whence == SEEK_SET)
			stream->_indicator = 0;
		_flaglseek = lseek(stream->_fileno, offset, whence);
		if(whence == SEEK_END)
			stream->_indicator = ENDOFFILE;
		if(_flaglseek == -1) {
			return _flaglseek;
		}
	}
	else {
		_flaglseek = lseek(stream->_fileno, offset, whence);
		if(_flaglseek == -1) {
			return _flaglseek;
		}
	}
	return 0;
}
	
//feof
int myfeof(MYFILE *stream) {
	int _count;
	if(stream->_fileno == 0 || stream->_fileno == 1 || stream->_fileno == 2)
		return 0;
	if(stream->_indicator == ENDOFFILE && stream->_buffindex == stream->_currchar)
		_count = 1;
	else if(stream->_buffer[stream->_currchar] == '\0' && stream->_buffindex != 0)
		if(stream->_readcount == 0)
			_count = 1;
		else
			_count = 0;
	else
		_count = 0;
	return _count;
}

//ftell
long myftell(MYFILE *stream) {
	off_t _count;
	if(stream->_fileno == 0 || stream->_fileno == 1 || stream->_fileno == 2)
		return -1;
	_count = lseek(stream->_fileno, -(stream->_buffindex - stream->_currchar), SEEK_CUR);
	lseek(stream->_fileno, (stream->_buffindex - stream->_currchar), SEEK_CUR);
	if(stream->_buffindex == stream->_currchar && stream->_buffindex != 0)
		_count += stream->_buffindex;
	if(stream->_indicator == ENDOFFILE) 
		if(stream->_buffindex)
		_count += 1;
	return _count;
}

//fgetpos
int myfgetpos(MYFILE *stream, fpos_t *pos) {
	int _flag = 0;
	long _count;
	if(stream->_fileno == 0 || stream->_fileno == 1 || stream->_fileno == 2)
		return -1;
	_count = (myftell(stream));
	pos->__pos = _count;
	return _flag;
}

//fsetpos
int myfsetpos(MYFILE *stream, const fpos_t *pos) {
	int _flag = 0, _flaglseek;
	long _count;
	if(stream->_fileno == 0 || stream->_fileno == 1 || stream->_fileno == 2)
		return -1;
	_count = pos->__pos;
	_flaglseek = myfseek(stream, _count, SEEK_SET);
	if(_flaglseek == -1)
		return _flaglseek;
	return _flag;
}










