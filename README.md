# File Library


NAME : GOURI MANOJ NANGLIYA
MIS : 111703018

DESCRIPTION :

Implementation of the FILE functions using the functions open, read, write, lseek, etc.
List of functions: fopen, fclose, fread, fwrite, fgetpos, fsetpos, feof, ftell, fseek.
Defined my own FILE typedef and then wrote the corresponding functions.
fread and fwrite are buffered except that buffering for fwrite for standard output is off.

RUN COMMANDS:
$ make
$ < input.txt ./project > output.txt



