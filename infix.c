#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include "cstack.h"
#include "stack.h"

#define NUM	100
#define OP	200
#define END	300
#define ERR	400

typedef struct token {
	int type;
	int val;
	char op; 
}token;
enum state { NUMBER, OPERATOR, FINISH, ERROR, SPC };
token gettoken(char *expr) {
	static int i = 0;
	static int no = 0;
	char currchar;
	static enum state currstate = SPC;
	enum state nextstate;
	token t;
	while(1) {
		currchar = expr[i];
		switch(currstate) {
			case NUMBER:
				switch(currchar) {
					case '0':case '1':case '2': case '3':
					case '4':case '5':case '6': case '7':
					case '8':case '9':
						nextstate = NUMBER;
						no = no * 10 + (currchar - '0');
						i++;
						break;
					case '+': case '-': case '*': case '/': case '%':
					case '(': case ')':
						nextstate = OPERATOR;
						t.type = NUM;
						t.val = no;
						currstate = nextstate;
						i++;
						return t;
						break;
					case '\0':
						nextstate = FINISH;
						t.type = NUM;
						t.val = no;
						currstate = nextstate;
						return t;
						break;
					case ' ':
						nextstate = SPC;
						t.type = NUM;
						t.val = no;
						currstate = nextstate;
						i++;
						return t;
						break;
					default:
						nextstate = ERROR;
						t.type = NUM;
						t.val = no;
						currstate = nextstate;
						return t;
						break;
				}
				break;
			case OPERATOR:
				switch(currchar) {
					case '0':case '1':case '2': case '3':
					case '4':case '5':case '6': case '7':
					case '8':case '9':
						no = currchar - '0';
						nextstate = NUMBER;
						t.type = OP;
						t.op = expr[i - 1];
						currstate = nextstate;
						i++;
						return t;
						break;
					case '+': case '-': case '*': case '/': case '%':
					case '(':case ')':
						nextstate = OPERATOR;
						t.type = OP;
						t.op = expr[i - 1];
						currstate = nextstate;
						i++;
						return t;
						break;
					case '\0':
						nextstate = FINISH;
						t.type = OP;
						t.op = expr[i - 1];
						currstate = nextstate;
						return t;
						break;
					case ' ':
						nextstate = SPC;
						t.type = OP;
						t.op = expr[i - 1];
						currstate = nextstate;
						i++;
						return t;
						break;
					default:
						nextstate = ERROR;
						t.type = OP;
						t.op = expr[i - 1];
						currstate = nextstate;
						return t;
					 	break;
				}
				break;
			case FINISH:
				t.type = END;
				return t;
				break;
			case ERROR:
				t.type = ERR;
				return t;
				break;
			case SPC:
				switch(currchar) {
					case '0':case '1':case '2': case '3':
					case '4':case '5':case '6': case '7':
					case '8':case '9':
						no = currchar - '0';
						nextstate = NUMBER;
						i++;
						break;
					case '+': case '-': case '*': case '/': case '%':
					case '(':case ')':
						nextstate = OPERATOR;
						i++;
						break;
					case '\0':
						nextstate = FINISH;
						break;
					case ' ':
						nextstate = SPC;
						i++;
						break;
					default:
						nextstate = ERROR;
						break;
				}
				currstate = nextstate;
				break;
		}
	}
}
void readline(char *line) {
	int i = 0;
	char ch = 'a';
	while(ch != '\n') {
		ch = getchar();
		line[i] = ch;
		i++;
	}
	line[i - 1] = '\0';
}

char ctop(cstack *cs) {
	char ch = 'g';
	if(!cisempty(cs)) {
		ch = cpop(cs);
		cpush(cs, ch);
	}
	return ch;
}
int precedence(char op) {
	switch(op) {
		case '+': case '-':
			return 1;
			break;	
		case '*': case '/':
			return 2;
			break;	
		case '%': 
			return 3;
			break;
		case '(':
			return 4;
			break;
		default:
			return 5;
			break;
	}
	return 5;
}

int infixeval(char *infix) {
	token t,prevt;
	stack s;
	cstack cs;
	int x, y, z;
	char currop, prevop;
	init(&s);
	cinit(&cs);
	while(1) {
		re:
		prevt = t;
		t = gettoken(infix);
		if(t.type == NUM) {
			if(!isfull(&s))
				push(&s, t.val);
			else {
				fprintf(stderr, "expression is too big\n");
				return INT_MIN;
			}
		}
		else if(t.type == OP) {
			currop = t.op;
			if(currop == '(') {
				if(prevt.type == OP) {
					if(!isfull(&s))
						cpush(&cs, currop);
					else {
						fprintf(stderr, "expression is too big\n");
						return INT_MIN;
					}
				}
				else {
					fprintf(stderr, "less operators\n");
					return INT_MIN;
				}
				goto re;
			}
			else if(currop == ')') {
				while(1) {
					prevop = ctop(&cs);
					if(prevop != '(') {
						if(!isempty(&s))
							prevop = cpop(&cs);
						else {
							fprintf(stderr, "less operators\n");
							return INT_MIN;
						}
						if(!isempty(&s))
							x = pop(&s);
						else {
							fprintf(stderr, "less operands\n");
							return INT_MIN;
						}
						if(!isempty(&s))
							y = pop(&s);
						else {
							fprintf(stderr, "less operands\n");
							return INT_MIN;
						}
						switch(prevop) {
							case '+':
								z = y + x;
								break;
							case '*':
								z = y * x;
								break;
							case '-':
								z = y - x;
								break;
							case '/':
								if(!x) {
									fprintf(stderr, "expression is too big\n");
									return INT_MIN;
								}
								else
									z = y / x;
								break;
							case '%':
								z = y % x;
								break;
							default:
								return INT_MIN;		
						}
						if(!isfull(&s))
							push(&s, z);
						else {
							fprintf(stderr, "expression is too big\n");
							return INT_MIN;
						}	
						
					}
					else if(prevop == '(') {
						if(!isempty(&s))
							prevop = cpop(&cs);
						else {
							fprintf(stderr, "less operators\n");
							return INT_MIN;
						}
						break;
					}
				}
				goto re;
			}
			prevop = ctop(&cs);
			if(prevop == 'g' || prevop == '(') {
				if(!isfull(&s))
					cpush(&cs, t.op);
				else {
					fprintf(stderr, "expression is too big\n");
					return INT_MIN;
				}
			}
			else {
				if(precedence(prevop) >= precedence(currop)) {
					if(!isempty(&s))
						prevop = cpop(&cs);
					else {
						fprintf(stderr, "less operators\n");
						return INT_MIN;
					}
					if(!isempty(&s))
						x = pop(&s);
					else {
						fprintf(stderr, "less operands\n");
						return INT_MIN;
					}
					if(!isempty(&s))
						y = pop(&s);
					else {
						fprintf(stderr, "less operands\n");
						return INT_MIN;
					}
					if(!isfull(&s))
						cpush(&cs, t.op);
					else {
						fprintf(stderr, "expression is too big\n");
						return INT_MIN;
					}
					switch(prevop) {
						case '+':
							z = y + x;
							break;
						case '*':
							z = y * x;
							break;
						case '-':
							z = y - x;
							break;
						case '/':
							if(!x) {
								fprintf(stderr, "expression is too big\n");
								return INT_MIN;
							}
							else
								z = y / x;
							break;
						case '%':
							z = y % x;
							break;
						default:
							return INT_MIN;		
					}		
					if(!isfull(&s))
						push(&s, z);
					else {
						fprintf(stderr, "expression is too big\n");
						return INT_MIN;
					}	
				}
				else {
					if(!isfull(&s))
						cpush(&cs, t.op);
					else {
						fprintf(stderr, "expression is too big\n");
						return INT_MIN;
					}
				}
			}
		}
		else if(t.type == END) {
			if(!cisempty(&cs)) {
				while(!cisempty(&cs)) {
					prevop = cpop(&cs);
					if(!isempty(&s))
						x = pop(&s);
					else {
						fprintf(stderr, "less operands\n");
						return INT_MIN;
					}
					if(!isempty(&s))
						y = pop(&s);
					else {
						fprintf(stderr, "less operands\n");
						return INT_MIN;
					}
					switch(prevop) {
						case '+':
							z = y + x;
							break;
						case '*':
							z = y * x;
							break;
						case '-':
							z = y - x;
							break;
						case '/':
							if(!x) {
								fprintf(stderr, "expression is too big\n");
								return INT_MIN;
							}
							else
								z = y / x;
							break;
						case '%':
							z = y % x;
							break;
						default:
							return INT_MIN;		
					}
				if(!isfull(&s))
					push(&s, z);
				else {	
					fprintf(stderr, "expression is too big\n");
					return INT_MIN;
				}
			}
			return z;
			}
			else {
				fprintf(stderr, "less operators\n");
				return INT_MIN;
			}
		}	
		else if(t.type == ERR) {
			fprintf(stderr, "Error in expression\n");
			return INT_MIN;
		}
	}

}
int main(int argc, char *argv[]) {
	char line[32];
	int result;
	readline(line);
	result = infixeval(line);
	if(result == INT_MIN)
		printf("error in expression\n");
	else
		printf("%d\n", result);
	return 0;
}
