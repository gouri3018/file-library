#include <stdio.h>
#include <string.h>
#include "file.h"

int main(int argc, char * argv[]) {
	MYFILE *fp, *fpr, *fpw, *fprw, *fpa, *fps;
	MYFILE *arr[30];
	char str[1000], str2[1000], ch;
	int n = 0, i, t, flag, var;
	long g;
	fpos_t pos;
	int indicator = 0;
	int arrint[20], arrint2[20];
	char file[25][5] = { "1.c", "2.c", "3.c", "4.c", "5.c", "6.c", "7.c", "8.c",
			"9.c", "10.c", "11.c", "12.c", "13.c", "14.c", "15.c", "16.c", 
			"17.c", "18.c", "19.c", "20.c", "21.c", "22.c", "23.c", "24.c", "25.c" };
		printf("\n");
	//standard input output
		//stdin
		//reading equal to given
		printf("CASE : stdin (reading equal to given data)\nOUTPUT:\t");
		myfread(str, sizeof(char), 5, STDIN);
		str[4] = '\0';
		if(!strcmp(str, "coep"))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n"); 

		//reading less than given
		printf("CASE : stdin (reading less than given data)\nOUTPUT:\t");
		myfread(str, sizeof(char), 4, STDIN);
		str[4] = '\0';
		if(!strcmp(str, "coep"))
			indicator++;
		myfread(str, sizeof(char), 9, STDIN);
		str[8] = '\0';
		if(!strcmp(str, " college"))
			indicator++;
		if(indicator == 2)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n"); 
		indicator = 0;

		//reading greater than bufmax
		printf("CASE : stdin (reading greater than bufmax)\nOUTPUT:\t");
		myfread(str, sizeof(char), 20, STDIN);
		str[20] = '\0';
		if(!strcmp(str, "coep college in pune"))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		
		//stdout
		strcpy(str, "coep college in pune");
		//writing less than given data
		printf("CASE : stdout (write less than given)\nOUTPUT:\t");
		n = myfwrite(str, sizeof(char), 4, STDOUT);
		if(n == 4)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//writing equal to given data
		printf("CASE : stdout (write equal to given)\nOUTPUT:\t");
		n = myfwrite(str, sizeof(char), 20, STDOUT);
		if(n == 20)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//stdin and stdout
		//stdin(read) = stdout(write)
		printf("CASE : stdin(read) = stdout(write)\nOUTPUT:\t");
		n = myfread(str, sizeof(char), 20, STDIN);
		if(n == 20)
			indicator++;
		n = myfwrite(str, sizeof(char), 20, STDOUT);
		if(n == 20)
			indicator++;
		if(indicator == 2)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;
	
		//stdin(read) = stdout(write)
		printf("CASE : stdin(read) > stdout(write)\nOUTPUT:\t");
		n = myfread(str, sizeof(char), 20, STDIN);
		if(n == 20)
			indicator++;
		n = myfwrite(str, sizeof(char), 4, STDOUT);
		if(n == 4)
			indicator++;
		if(indicator == 2)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;

		//feof, ftell, fgetpos, fsetpos, fseek
		printf("CASE : feof, ftell, fgetpos, fsetpos, fseek with std input\nOUTPUT:\t");
		n = myfread(str, sizeof(char), 4, STDIN);
		if(n == 4)
			indicator++;
		g = myfseek(STDIN, 0, SEEK_SET);
		if(g == -1)
			indicator++;
		n = myfgetpos(STDIN, &pos);
		if(n == -1)
			indicator++;
		n = myfsetpos(STDIN, &pos);
		if(n == -1)
			indicator++;
		g = myfeof(STDIN);
		if(g == 0)
			indicator++;
		g = myftell(STDIN);
		if(g == -1)
			indicator++;
		if(indicator == 6)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;

		//feof, ftell, fgetpos, fsetpos, fseek
		printf("CASE : feof, ftell, fgetpos, fsetpos, fseek with std output\nOUTPUT:\t");
		n = myfwrite("coep", sizeof(char), 4, STDOUT);
		if(n == 4)
			indicator++;
		g = myfseek(STDOUT, 0, SEEK_SET);
		if(g == -1)
			indicator++;
		n = myfgetpos(STDOUT, &pos);
		if(n == -1)
			indicator++;
		n = myfsetpos(STDOUT, &pos);
		if(n == -1)
			indicator++;
		g = myfeof(STDOUT);
		if(g == 0)
			indicator++;
		g = myftell(STDOUT);
		if(g == -1)
			indicator++;
		if(indicator == 6)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;

		//one with stderr
		printf("CASE : stderr\nOUTPUT:\t");
		n = myfwrite("no error", sizeof(char), 8, STDERR);
		if(n == 8)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

	//myfopen cases:
		//opening file in read mode
		printf("CASE : open a file in r mode\nOUTPUT:\t");
		fp = myfopen("test.txt", "r");
		if(fp != NULL)
			indicator++;
		flag = myfclose(fp);
		if(flag == 0)
			indicator++;
		if(indicator == 2)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;
		
		//opening a non exitsent file in write mode
		printf("CASE : open a non-existent file in w mode\nOUTPUT:\t");
		fp = myfopen("notexist.txt", "w");
		if(fp != NULL)
			indicator++;
		flag = myfclose(fp);
		if(flag == 0)
			indicator++;
		if(indicator == 2)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;

		//opening MAX no. of files at a time 
		printf("CASE : open MAX no. of text files same time in w mode\nOUTPUT:\t");
		for(i = 0; i < 20; i++) {
			arr[i] = myfopen(file[i], "w");
			if(arr[i] != NULL) {
				indicator++;
			}
		}
		for(i = 0; i < 20; i++) {
			flag = myfclose(arr[i]);
			if(flag == 0) {
				indicator++;
			}
		}
		if(indicator == 40)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;

		//opening and closing of MAX no. of files in random order
		printf("CASE : open MAX no. of files same time in random order in w mode\nOUTPUT:\t");
		for(i = 0; i < 20; i++) {
			arr[i] = myfopen(file[i], "w");
			if(arr[i] != NULL) {
				indicator++;
			}
		}
		for(i = 0; i < 5; i++) {
			flag = myfclose(arr[i]);
			if(flag == 0) {
				indicator++;
			}
		}
		for(i = 0; i < 5; i++) {
			arr[i] = myfopen(file[i], "w");
			if(arr[i] != NULL) {
				indicator++;
			}
		}
		for(i = 0; i < 20; i++) {
			flag = myfclose(arr[i]);
			if(flag == 0) {
				indicator++;
			}
		}
		if(indicator == 50)
			printf("PASS\n");
		else 
			printf("FAIL\n");
		printf("\n");
		indicator = 0;

		//opening more than MAX no. of files at a time 
		printf("CASE : open more than MAX no. of files in w mode\nOUTPUT:\t");
		for(i = 0; i < 21; i++) {
			arr[i] = myfopen(file[i], "w");
			if(arr[i] != NULL) {
				indicator++;
			}
		}
		for(i = 0; i < 21; i++) {
			flag = myfclose(arr[i]);
			if(flag == 0) {
				indicator++;
			}
		}
		if(indicator == 40)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;

		//opening a non existent file in read mode
		printf("CASE : open a non existent file in r mode\nOUTPUT:\t");
		arr[22] = myfopen("coep", "r");
		if(arr[22] == NULL)
			indicator++;
		if(indicator == 1)
			printf("PASS\n");
		printf("\n");
		indicator = 0;

		//opening a file in incorrect mode
		printf("CASE : open a file in incorrect mode\nOUTPUT:\t");
		arr[23] = myfopen("folder", "g");
		if(arr[23] == NULL)
			indicator++;
		if(indicator == 1)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;

		
		//opening a binary file in rb mode(all other cases like wb, w+b, etc, are same like w, w+, etc.)
		printf("CASE : open a binary file in rb mode\nOUTPUT:\t");
		fpr = myfopen("binary1.txt", "rb");
		if(fpr != NULL)
			printf("PASS\n");
		else
			printf("FAIL\n");
		myfclose(fpr);
		printf("\n");
	


	//fread and fwrite:
		//reading from a text file less than bufmax
		printf("CASE : read from a text file less than bufmax\nOUTPUT:\t");
		fpr = myfopen("testr.txt", "r");
		n = myfread(str, sizeof(char), 10, fpr);
		str[n] = '\0';
		myfclose(fpr);
		if(!strcmp(str, "Pune is kn"))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
	
		//reading from a text file greater than bufmax
		printf("CASE : read from a text file greater than bufmax\nOUTPUT:\t");
		fpr = myfopen("testr.txt", "r");
		n = myfread(str, sizeof(char), 30, fpr);
		str[n] = '\0';
		myfclose(fpr);
		if(!strcmp(str, "Pune is known as Education Hub"))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		
		//reading from a text file equal to bufmax
		printf("CASE : read from a text file equal than bufmax\nOUTPUT:\t");
		fpr = myfopen("testr.txt", "r");
		n = myfread(str, sizeof(char), 12, fpr);
		str[n] = '\0';
		myfclose(fpr);
		if(!strcmp(str, "Pune is know"))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
	
		//reading a file character by character
		printf("CASE : read a file character by character\nOUTPUT:\t");
		fpr = myfopen("infix.c", "r");
		n = myfread(&ch, sizeof(char), 1, fpr);
		indicator += n;
		while(n) {
			n = myfread(&ch, sizeof(char), 1, fpr);
			indicator += n;
		}
		t = myftell(fpr);
		myfclose(fpr);
		if(indicator == t)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;

		//reading from a text file who has less data than asked
		printf("CASE : read from a text file who has less data than asked\nOUTPUT:\t");
		fpr = myfopen("testr2.txt", "r");
		n = myfread(str, sizeof(char), 16, fpr);
		str[n - 1] = '\0';
		myfclose(fpr);
		if(!strcmp(str, "pune."))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		strcpy(str, "coep is good college in pune.");
		
		//writing into a file opened in w mode
		printf("CASE : write ina file opened in w mode\nOUTPUT:\t");
		fpw = myfopen("testww.txt", "w");
		myfwrite("123456789", sizeof(char), 9, fpw);
		myfclose(fpw);
		fpw = myfopen("testww.txt", "r");
		myfread(str, sizeof(char), 9, fpw);
		str[9] = '\0';
		myfclose(fpw);
		if(!strcmp(str, "123456789"))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//only writing into a text file less than bufmax
		strcpy(str, "coep is good college");
		printf("CASE : write to a text file less than BUFMAX\nOUTPUT:\t");
		fpw = myfopen("testw1.txt", "w+");
		myfwrite(str, sizeof(char), 10, fpw);
		myfseek(fpw, 0, SEEK_SET);
		n = myfread(str2, sizeof(char), 10, fpw);
		str2[n] = '\0';
		myfclose(fpw);
		if(!strcmp(str2, "coep is go"))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		
		//only writing into a file text greater than bufmax
		strcpy(str, "coep is good college");
		printf("CASE : write to a text file greater than BUFMAX\nOUTPUT:\t");
		fpw = myfopen("testw2.txt", "w+");
		myfwrite(str, sizeof(char), 20, fpw);
		myfseek(fpw, 0, SEEK_SET);
		n = myfread(str2, sizeof(char), 20, fpw);
		str2[n] = '\0';
		myfclose(fpw);
		if(!strcmp(str2, "coep is good college"))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
	
		//only writing into a file text equal than bufmax
		strcpy(str, "coep is good college");
		printf("CASE : write to a text file equal than BUFMAX\nOUTPUT:\t");
		fpw = myfopen("testw3.txt", "w+");
		myfwrite(str, sizeof(char),12, fpw);
		myfseek(fpw, 0, SEEK_SET);
		n = myfread(str2, sizeof(char), 12, fpw);
		str2[n] = '\0';
		myfclose(fpw);
		if(!strcmp(str2, "coep is good"))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//reading and writing at same time in two different files less than read 
		printf("CASE : read and write at same time in two different files\nOUTPUT:\t");
		fpr = myfopen("testr.txt", "r");
		fpw = myfopen("testw4.txt", "w+");
		n = myfread(str, sizeof(char), 10, fpr);
		str[n] = '\0';
		myfwrite(str, sizeof(char), 10, fpw);
		myfseek(fpw, 0, SEEK_SET);
		n = myfread(str2, sizeof(char), 10, fpw);
		str2[n] = '\0';
		myfclose(fpr);
		myfclose(fpw);
		if(!strcmp(str2, str))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
	
		//both reading and writing into a file at same time r+w=max
		printf("CASE : read and write into same file s.t. r + w = MAX\nOUTPUT:\t");
		fprw = myfopen("testrw1.txt", "r+");
		n = myfread(str, sizeof(char), 8, fprw);
		str[n] = '\0';
		myfwrite("1111", sizeof(char), 4, fprw);
		myfseek(fprw, 0, SEEK_SET);
		n = myfread(str2, sizeof(char), 12, fprw);
		str2[n] = '\0';
		strcat(str, "1111");
		myfclose(fprw);
		if(!strcmp(str2, str))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//both reading and writing into a file at same time r+w>max r>w
		printf("CASE : read and write in same file s.t. r + w > MAX , r > w\nOUTPUT:\t");
		fprw = myfopen("testrw2.txt", "r+");
		n = myfread(str, sizeof(char), 10, fprw);
		str[n] = '\0';
		myfwrite("1111", sizeof(char), 4, fprw);
		myfclose(fprw);
		fprw = myfopen("testrw2.txt", "r");
		n = myfread(str2, sizeof(char), 21, fprw);
		str2[n] = '\0';
		myfclose(fprw);
		if(!strcmp(str2, "coep is go1111ollege."))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//both reading and writing into a file at same time r+w>max r<w
		printf("CASE : read and write in same file s.t. r + w > MAX , r < w\nOUTPUT:\t");
		fprw = myfopen("testrw3.txt", "r+");
		n = myfread(str, sizeof(char), 4, fprw);
		str[n] = '\0';
		myfwrite("college of engineering pune", sizeof(char), 10, fprw);
		myfseek(fprw, 0, SEEK_SET);
		n = myfread(str2, sizeof(char), 14, fprw);
		str2[n] = '\0';
		strcat(str, "college of");
		myfclose(fprw);
		if(!strcmp(str2, str))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//both reading and writing into a file at same time r+w>max r>max w>max
		printf("CASE : read and write in same file s.t. r + w > MAX , r > MAX, w > MAX\nOUTPUT:\t");
		fprw = myfopen("testrw4.txt", "r+");
		n = myfread(str, sizeof(char), 15, fprw);
		str[n] = '\0';
		myfwrite("college of engineering pune", sizeof(char), 16, fprw);
		myfseek(fprw, 0, SEEK_SET);
		n = myfread(str2, sizeof(char), 31, fprw);
		str2[n] = '\0';
		strcat(str, "college of engin");
		myfclose(fprw);
		if(!strcmp(str2, str))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//writing in file opened in read mode
		printf("CASE : write in file opened in r mode\nOUTPUT:\t");
		fpr = myfopen("test.txt", "r");
		n = myfwrite("1111", sizeof(char), 4, fpr);
		myfclose(fpr);
		if(n == 0)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//file opening with append mode 
		printf("CASE : file in \"a+\" mode\nOUTPUT:\t");
		fpa = myfopen("testa2.txt", "a+");
		n = 1;
		i = 0;
		while(n != 0) {
			n = myfread(&str2[i], 1, 1, fpa);
			i++;
		}
		str2[i - 1] = '\0';
		myfwrite("1234", sizeof(char), 4, fpa);
		myfseek(fpa, 0, SEEK_SET);
		n = 1;
		i = 0;
		while(n != 0) {
			n = myfread(&str[i], 1, 1, fpa);
			i++;
		}
		str[i - 1] = '\0';
		myfclose(fpa);
		strcat(str2, "1234");
		if(!strcmp(str, str2))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
	
		//file in a mode
		printf("CASE : file in \"a\" mode\nOUTPUT:\t");
		fpa = myfopen("testa1.txt", "r");
		i = 0;
		n = 1;
		while(n != 0){
			n = myfread(&str2[i], sizeof(char), 1, fpa);
			i++;
		}
		str2[i - 1] = '\0';
		indicator = i;
		myfclose(fpa);
		fpa = myfopen("testa1.txt", "a");
		myfwrite("1234", sizeof(char), 4, fpa);
		myfclose(fpa);
		fpa = myfopen("testa1.txt", "r");
		i = 0;
		n = 1;
		while(n != 0){
			n = myfread(&str[i], sizeof(char), 1, fpa);
			i++;
		}
		str[i - 1] = '\0';
		myfclose(fpa);
		strcat(str2, "1234");
		if(!strcmp(str, str2))
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;

	//fseek feof and ftell with fread and fwrite
		//read and then SEEK_SET
		printf("CASE : read and then SEEK_SET\nOUTPUT:\t");
		fps = myfopen("tests1.txt", "r");
		n = myfread(str, sizeof(char), 16, fps);
		str[n] = '\0';
		g = myftell(fps);
		if(g == 16)
			indicator++;
		myfseek(fps, 0, SEEK_SET);
		g = myftell(fps);
		if(g == 0)
			indicator++;
		n = myfread(str, sizeof(char), 6, fps);
		str[n] = '\0';
		g = myftell(fps);
		if(g == 6)
			indicator++;
		myfseek(fps, 5, SEEK_SET);
		n = myfread(str, sizeof(char), 15, fps);
		str[n] = '\0';
		g = myftell(fps);
		if(g == 20)
			indicator++;
		t = myfeof(fps);
		if(t == 0)
			indicator++;
		myfclose(fps);
		if(indicator == 5)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;

		//read and write with SEEK_SET
		printf("CASE : read and write then SEEK_SET\nOUTPUT:\t");
		fps = myfopen("tests2.txt", "w+");
		myfwrite("college of engineering pune", sizeof(char), 16, fps);
		myfseek(fps, 8, SEEK_SET);
		g = myftell(fps);
		if(g == 8)
			indicator++;
		n = myfread(str, sizeof(char), 2, fps);
		str[n] = '\0';
		myfwrite("1111", sizeof(char), 2, fps);
		myfseek(fps, 0, SEEK_SET);
		n = myfread(str, sizeof(char), 7, fps);
		str[n] = '\0';
		g = myftell(fps);
		if(g == 7)
			indicator++;
		t = myfeof(fps);
		if(t == 0)
			indicator++;
		myfseek(fps, 0, SEEK_SET);
		n = myfread(str2, sizeof(char), 16, fps);
		str2[n] = '\0';
		myfclose(fps);
		if(!strcmp(str2, "college of11ngin") && indicator == 3)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;
	
		//write with SEEK_SET
		printf("CASE : write then SEEK_SET\nOUTPUT:\t");
		fps = myfopen("tests3.txt", "w+");
		myfwrite("college of engineering pune", sizeof(char), 16, fps);
		g = myftell(fps);
		if(g == 16)
			indicator++;
		myfseek(fps, 8, SEEK_SET);
		g = myftell(fps);
		if(g == 8)
			indicator++;
		myfwrite("college of engineering pune", sizeof(char), 5, fps);
		myfseek(fps, 0, SEEK_SET);
		myfwrite("1111", sizeof(char), 4, fps);
		g = myftell(fps);
		if(g == 4)
			indicator++;
		t = myfeof(fps);
		if(t == 1)
			indicator++;
		myfseek(fps, 0, SEEK_SET);
		n = myfread(str2, sizeof(char), 16, fps);
		str2[n] = '\0';
		myfclose(fps);
		if(!strcmp(str2, "1111ege collegin") && indicator == 4)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;
		
		//read and write with SEEK_END
		printf("CASE : write then SEEK_END\nOUTPUT:\t");
		fps = myfopen("tests4.txt", "r+");
		n = myfread(str, sizeof(char), 8, fps);
		g = myftell(fps);
		if(g == 8)
			indicator++;
		myfseek(fps, 0, SEEK_SET);
		g = myftell(fps);
		if(g == 0)
			indicator++;
		myfwrite("1111", sizeof(char), 4, fps);
		g = myftell(fps);
		if(g == 4)
			indicator++;
		myfseek(fps, 0, SEEK_END);
		t = myfeof(fps);
		if(t == 1)
			indicator++;
		n = myfread(str, sizeof(char), 1, fps);
		if(n == 0)
			indicator++;
		myfseek(fps, -4, SEEK_END);
		g = myftell(fps);
		if(g == 18)
			indicator++;
		n = myfread(str, sizeof(char), 4, fps);
		g = myftell(fps);
		if(g == 22)
			indicator++;
		t = myfeof(fps);
		if(t == 0)
			indicator++;
		myfseek(fps, 0, SEEK_SET);
		n = myfread(str2, sizeof(char), 21, fps);
		str2[n] = '\0';
		myfclose(fps);
		if(!strcmp(str2, "1111 is good college.") && indicator == 8)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;
		
		
		//read and write with SEEK_CUR
		printf("CASE : read and write then SEEK_CUR\nOUTPUT:\t");
		fps = myfopen("tests5.txt", "r+");
		n = myfread(str, sizeof(char), 8, fps);
		str[n] = '\0';
		g = myftell(fps);
		if(g == 8)
			indicator++;
		myfseek(fps, 4, SEEK_CUR);
		g = myftell(fps);
		if(g == 12)
			indicator++;
		myfwrite("1111", sizeof(char), 4, fps);
		g = myftell(fps);
		if(g == 16)
			indicator++;
		myfseek(fps, -16, SEEK_CUR);
		g = myftell(fps);
		if(g == 0)
			indicator++;
		n = myfread(str, sizeof(char), 16, fps);
		str[n] = '\0';
		g = myftell(fps);
		if(g == 16)
			indicator++;
		myfseek(fps, 0, SEEK_SET);
		g = myftell(fps);
		if(g == 0)
			indicator++;
		t = 0;
		n = myfread(&ch, sizeof(char), 1, fps);
		while(!myfeof(fps)) {
			t++;
			n = myfread(&ch, sizeof(char), 1, fps);
		}
		g = myftell(fps);
		if(g == t)
			indicator++;
		myfseek(fps, 0, SEEK_SET);
		n = myfread(str2, sizeof(char), 21, fps);
		str2[n] = '\0';
		myfclose(fps);
		if(!strcmp(str2, "coep is good1111lege.") && indicator == 7)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;
		
		//incorrect whence
		printf("CASE : incorrect whence in fseek\nOUTPUT:\t");
		fps = myfopen("test.txt", "r");
		n = myfseek(fps, 0, EOF);
		myfclose(fps);
		if(n == -1)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");	
		
		//file has less data than buffer and feof is used
		printf("CASE : file has less data than buffer and feof is used\nOUTPUT:\t");
		fpr = myfopen("testf.txt", "r");
		n = myfread(str, sizeof(char), 12, fpr);
		str[n] = '\0';
		t = myfeof(fpr);
		if(t == 0)
			indicator++;
		myfclose(fpr);
		if(!strcmp(str, "coep..\n") && indicator == 1)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");	
		indicator = 0;
		
	
	//fgetpos and fsetpos
		//end, start, middle
		printf("CASE : fgetpos and fsetpos\nOUTPUT:\t");
		fps = myfopen("testf2.txt", "r+");
		var = myfgetpos(fps, &pos);
		if(var == 0)
			indicator++;
		n = myfread(str, sizeof(char), 4, fps);
		myfsetpos(fps, &pos);
		t = myftell(fps);
		if(t == 0)
			indicator++;
		myfwrite("coep", sizeof(char), 4, fps);
		myfseek(fps, 4, SEEK_CUR);
		var = myfgetpos(fps, &pos);
		if(var == 0)
			indicator++;
		myfwrite("1111", sizeof(char), 4, fps);
		myfsetpos(fps, &pos);
		t = myftell(fps);
		if(t == 8)
			indicator++;
		myfwrite("2222", sizeof(char), 4, fps);
		myfseek(fps, 0, SEEK_SET);
		var = myfgetpos(fps, &pos);
		if(var == 0)
			indicator++;
		n = myfread(str, sizeof(char), 15, fps);
		var = myfgetpos(fps, &pos);
		if(var == 0)
			indicator++;
		myfwrite("1111", sizeof(char), 4, fps);
		myfsetpos(fps, &pos);
		t = myftell(fps);
		if(t == 15)
			indicator++;
		myfwrite("2222", sizeof(char), 4, fps);
		myfseek(fps, 0, SEEK_SET);
		n = myfread(str2, sizeof(char), 21, fps);
		str2[n] = '\0';
		myfclose(fps);
		if(!strcmp(str2, "coep is 2222 co2222e.") && indicator == 7)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;
	
	//binary files
		//reading from binary file less than buffmax
		printf("CASE : read from binary file less than bufmax\nOUTPUT:\t");
		fpr = myfopen("binary1.txt", "r");
		myfread(&arrint[0], sizeof(int), 1, fpr);
		myfclose(fpr);
		if(arrint[0] == 1)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//reading from binary file greater than buffmax
		printf("CASE : read from binary file greater than bufmax\nOUTPUT:\t");
		fpr = myfopen("binary1.txt", "r");
		myfread(arrint, sizeof(int), 4, fpr);
		myfclose(fpr);
		if(arrint[0] == 1 && arrint[1] == 2 && arrint[2] == 3 && arrint[3] == 4)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//reading from binary file equal to buffmax
		printf("CASE : read from binary file equal to bufmax\nOUTPUT:\t");
		fpr = myfopen("binary1.txt", "r");
		myfread(arrint, sizeof(int), 3, fpr);
		myfclose(fpr);
		if(arrint[0] == 1 && arrint[1] == 2 && arrint[2] == 3)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		for(n = 0;n < 4; n++) {
			arrint[n] = n;
		}

		//writing to binary file less than buffmax
		printf("CASE : write to a binary file less than bufmax\nOUTPUT:\t");
		fpw = myfopen("binary2.txt", "w+");
		myfwrite(&arrint[0], sizeof(int), 1, fpw);
		myfseek(fpw, 0, SEEK_SET);
		myfread(&arrint[5], sizeof(int), 1, fpw);
		myfclose(fpw);
		if(arrint[5] == arrint[0])
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//writing to binary file greater than buffmax
		printf("CASE : write to a binary file greater than bufmax\nOUTPUT:\t");
		fpw = myfopen("binary2.txt", "w");
		myfwrite(arrint, sizeof(int), 4, fpw);
		myfclose(fpw);
		fpw = myfopen("binary2.txt", "r");
		for(n = 0; n < 4; n++)
			myfread(&arrint[5 + n], sizeof(int), 1, fpw);
		myfclose(fpw);
		for(n = 0; n < 4; n++)
			if(arrint[n] != arrint[n + 5])
				indicator++;
		if(!indicator)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//writing to binary file equal to buffmax
		printf("CASE : write to a binary file equal to bufmax\nOUTPUT:\t");
		fpw = myfopen("binary2.txt", "w");
		myfwrite(arrint, sizeof(int), 3, fpw);
		myfclose(fpw);
		fpw = myfopen("binary2.txt", "r");
		for(n = 0; n < 3; n++)
			myfread(&arrint[5 + n], sizeof(int), 1, fpw);
		myfclose(fpw);
		for(n = 0; n < 3; n++)
			if(arrint[n] != arrint[n + 5])
				indicator++;
		if(!indicator)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//both reading and writing into a binary file at same time r+w=max
		printf("CASE : read and write into same binary file s.t. r + w = MAX\nOUTPUT:\t");
		indicator = 0;
		fprw = myfopen("binaryrw1.txt", "r+");
		myfread(arrint, sizeof(int), 2, fprw);
		myfwrite(&arrint[2], sizeof(int), 1, fprw);
		myfseek(fprw, 0, SEEK_SET);
		myfread(arrint2, sizeof(int), 3, fprw);
		for(n = 0; n < 3; n++)
			if(arrint[n] != arrint2[n])
				indicator++;
		myfclose(fprw);
		if(!indicator)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		for(n = 0;n < 4; n++) {
			arrint[n] = n;
		}
		//both reading and writing into a binary file at same time r+w>max
		printf("CASE : read and write in same binary file s.t. r + w > MAX\nOUTPUT:\t");
		indicator = 0;
		fprw = myfopen("binaryrw2.txt", "r+");
		myfread(arrint, sizeof(int), 2, fprw);
		myfwrite(arrint, sizeof(int), 2, fprw);
		myfseek(fprw, 0, SEEK_SET);
		myfread(arrint2, sizeof(int), 4, fprw);
		myfclose(fprw);
		for(n = 0; n < 4; n++)
			if(arrint[n % 2] != arrint2[n])
				indicator++;
		if(!indicator)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//both reading and writing into a binary file at same time r+w>max r>max w>max
		printf("CASE : read and write in same binary file s.t. r + w > MAX , r > MAX, w > MAX\nOUTPUT:\t");
		indicator = 0;
		fprw = myfopen("binaryrw3.txt", "r+");
		myfread(arrint, sizeof(int), 4, fprw);
		myfwrite(arrint, sizeof(int), 4, fprw);
		myfseek(fprw, 0, SEEK_SET);
		myfread(arrint2, sizeof(int), 8, fprw);
		myfclose(fprw);
		for(n = 0; n < 8; n++) {
			if(arrint[n % 4] != arrint2[n])
				indicator++;}
		if(!indicator)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");
		indicator = 0;

	//fseek with binary files
		//read and write with SEEK_SET
		printf("CASE : read and write with SEEK_SET in binary file\nOUTPUT:\t");
		arrint[0] = 1;
		fprw = myfopen("binarys1.txt", "w+");
		myfwrite(&arrint[0], sizeof(int), 1, fprw);
		myfseek(fprw, 0, SEEK_SET);
		myfread(&arrint[1], sizeof(int), 1, fprw);
		myfclose(fprw);
		if(arrint[1] == arrint[0])
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");  
		
		//read and write with SEEK_CUR
		printf("CASE : read and write with SEEK_CUR in binary file\nOUTPUT:\t");
		fprw = myfopen("binarys2.txt", "r");
		myfread(arrint, sizeof(int), 2, fprw);
		myfseek(fprw, -sizeof(int), SEEK_CUR);
		myfread(&arrint[0], sizeof(int), 1, fprw);
		myfclose(fprw);
		if(arrint[0] == 2)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n");

		//read and write with SEEK_END
		printf("CASE : read and write with SEEK_END in binary file\nOUTPUT:\t");
		n = 5;
		i = 0;
		fpr = myfopen("binarys3.txt", "r");
		while(myfeof(fpr)) {
			myfread(&arrint[i], sizeof(int), 1, fpr);
			i++;
		}
		indicator = i - 1;
		myfclose(fpr);
		fprw = myfopen("binarys3.txt", "w+");
		n = 5;
		myfseek(fprw, 0, SEEK_END);
		myfwrite(&n, sizeof(int), 1, fprw);
		myfseek(fprw, 0, SEEK_SET);
		i = 0;
		while(myfeof(fprw)) {
			myfread(&arrint2[i], sizeof(int), 1, fprw);
			i++;
		}
		myfclose(fprw);
		n = indicator;
		indicator = 0;
		for(i = 0; i < n; i++)
			if(arrint[i] == arrint2[i])
				indicator++;
		if(arrint2[n] == 5)
			indicator++;
		if(indicator == n + 1)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n"); 
		indicator = 0;
	
		//feof, ftell, fgetpos and fsetpos
		printf("CASE : feof, ftell, fsetpos, fgetpos with a binary file\nOUTPUT:\t");
		fprw = myfopen("binaryf.txt", "r+");
		g = myftell(fprw);
		if(g == 0)
			indicator++;
		myfgetpos(fprw, &pos);
		myfread(&arrint[0], sizeof(int), 1, fprw);
		myfsetpos(fprw, &pos);
		g = myftell(fprw);
		if(g == 0)
			indicator++;
		myfread(&arrint[0], sizeof(int), 1, fprw);
		myfgetpos(fprw, &pos);
		myfseek(fprw, 0, SEEK_END);
		t = myfeof(fprw);
		if(t != 0)
			indicator++;
		myfsetpos(fprw, &pos);
		myfread(&arrint[1], sizeof(int), 1, fprw);
		if(arrint[1] == 2)
			indicator++;
		myfclose(fprw);
		if(indicator == 4)
			printf("PASS\n");
		else
			printf("FAIL\n");
		printf("\n"); 
		indicator = 0;

		return 0;
}
















