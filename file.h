#define BUFMAX 12
#include <stdio.h>
typedef struct MYFILE {
	int _fileno;   //file descriptor
	int _flagsize;
	char *_arg;     //mode
	int _readcount;  //count no of bytes read
	int _usage;  //file is used or not
	char _buffer[BUFMAX];
	int _buffindex;
	int _currchar;
	int _indicator;
}MYFILE;

typedef long unsigned int _size;
enum _use { _NOTINUSE , _INUSE };

extern MYFILE _stdin;
extern MYFILE _stdout;
extern MYFILE _stderr;
#define STDIN &_stdin
#define STDOUT &_stdout
#define STDERR &_stderr

//fopen
MYFILE *myfopen(char *filename, char *mode);

//fclose
int myfclose(MYFILE *stream);

//fread
_size myfread(void *ptr, _size size, _size n, MYFILE *stream);

//fwrite
_size myfwrite(void *ptr, _size size, _size n, MYFILE *stream);

//ftell
long myftell(MYFILE *stream);

//fseek
int myfseek(MYFILE *stream, long offset, int whence);

//fgetpos
int myfgetpos(MYFILE *stream, fpos_t *pos);

//fsetpos
int myfsetpos(MYFILE *stream, const fpos_t *pos);

//feof
int myfeof(MYFILE *stream);
